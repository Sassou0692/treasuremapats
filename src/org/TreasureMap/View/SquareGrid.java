package org.TreasureMap.View;

import java.util.List;

import javax.swing.JPanel;

import javax.swing.GroupLayout;

abstract class SquareGrid extends JPanel {

	// // // // // ATTRIBUTES
	// Accessible attributes
	protected int nbRows;
	protected int nbCols;
	protected int size;
	protected boolean adjustable;
	protected boolean interactable;
	protected boolean exclusive;
	protected List<int[]> coordActives;
	protected List<JPanel> actives;
	// Unaccessible attributes
	protected JPanel panel;
	protected GroupLayout layout;
	
	// // // // // CONSTRUCTORS
	SquareGrid(int nbRows, int nbCols, int squareSize)
	{
		// JPanel constructor
		super();
		// Initialize the accessible attributes
		this.nbRows = nbRows;
		this.nbCols = nbCols;
		size = squareSize;
		// Initialize the unaccessible attributes
		panel = this;
		layout = new GroupLayout(panel);
		panel.setLayout(layout);
	}
	
	// // // // // GETTERS
	public int getNumberRows()
	{ return nbRows; }
	public int getNumberCols()
	{ return nbCols; }
	public int[] getDimension()
	{ return new int[] { nbRows, nbCols }; }
	public int getSideSize()
	{ return size; }
	public boolean isAdjustable()
	{ return adjustable; }
	public boolean isInteractable()
	{ return interactable; }
	public boolean isExclusive()
	{ return exclusive; }
	public int[][] getActives()
	{ return actives.toArray(new int[0][]); }
	
	// // // // // SETTERS
	
	// // // // // METHODS
	// Method to instantiate the cells and add them to the Parallel Group (row or column)
	protected GroupLayout.ParallelGroup celler(GroupLayout.ParallelGroup grp, int idx1, int idx2)
	{ return grp; }
	// Method to set the square grid layout with the instantiated cells
	protected void squarer()
	{
		// Set the horizontal groups
		GroupLayout.SequentialGroup sgRow = layout.createSequentialGroup();
		GroupLayout.ParallelGroup pgCol;
		for (int i = 0; i < nbCols; i = i + 1)
		{
			pgCol = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
			for (int j = 0; j < nbRows; j = j + 1)
			{ pgCol = celler(pgCol, j, i); }
			sgRow.addGroup(pgCol);
		}
		layout.setHorizontalGroup(sgRow);
		// Set the vertical groups
		GroupLayout.SequentialGroup sgCol = layout.createSequentialGroup();
		GroupLayout.ParallelGroup pgRow;
		for (int i = 0; i < nbRows; i = i + 1)
		{
			pgRow = layout.createParallelGroup(GroupLayout.Alignment.BASELINE);
			for (int j = 0; j < nbCols; j = j + 1)
			{ pgRow = celler(pgRow, i, j); }
			sgCol.addGroup(pgRow);
		}
		layout.setVerticalGroup(sgCol);
	}
	
}
