package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Font;
// Imports Java Utilities
import java.util.HashMap;
import java.awt.color.*;
import java.awt.Font;

class Scene5 extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;

	private JButton next;
	
	// // // // // CONSTRUCTORS
	Scene5(int width, int height) {
		// Scene constructor
		super("FifthScene", "key", width, height);

		// Initialize JTextArea text
		text = new JTextArea(
				" I have the key, i can unlock this door  !\n ");
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setBackground(Color.black);
		text.setForeground(Color.white);
		parts.put("Text", text);
		text.setBounds(230, 700, 370, 40);
		layers.add(text, new Integer(10));
		// Initialize JButton next
		next = new JButton("Unlock");
		next.setBackground(Color.black);
		next.setForeground(Color.white);
		parts.put(getSceneName() + "Validate", next);
		next.setBounds(345, 550, 100, 50);
		layers.add(next, new Integer(10));
		
	}

}
