package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
// Imports AWT
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;


public class Scene2 extends Scene {
	
	// // // // // ATTRIBUTES
	private JTextArea text;
	private JTextArea enigme1;
	private JTextArea enigme2;
	private JTextArea text2;
	private JTextField playerChoice;
	private JButton validate;
	private ArrayList<JTextArea> sc2Texts;
	
	// // // // // CONSTRUCTORS
	Scene2(int width, int height) 
	{
		// Scene constructor
		super("SecondScene", "dragon", width, height);
		// Initialize JTextArea text
		String[] texts = new String[] {
				"           ARRRGGHHHH !!!\n"
						+ " What animal dares to sail on my seas ?? \n "
						+ " If you are able to answer my first question,\n"
						+ " you will understand why this island is \n"
						+ " a prized pirate destination...\n"
						+ " Or you will end like the others as my meals... \n"
						+ "         Muhwahahaha ! \n", 
						
						" A flying dragon look for it.\n" + 
						" A roaring dragon holds it in their talons.\n" + 
						" A sleeping dragon imagine it in their dreams.\n" + 
						" But it is there, Come and steal it from me,\n"
						+ " if you dare..." ,
						
						"   Well done, answer my second question and \n"
						+ "i might give you something for your adventure \n"
						+ "                                  -----                       \n "
						+ "I'm as fragil as when i'm pronounced, i die...",
						
						"                  ARRRRGHHH !!     \n"
						+ "    You are so strong, take this umbrella, \n "
						+ "             it might help you..."
						
		};
		sc2Texts = new ArrayList<JTextArea>();
		
		text = new JTextArea(texts[0]);
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setForeground(Color.white);
		text.setBackground(new Color(0, 0, 255, 155));
		parts.put(getSceneName() + "TA", text);
		text.setBounds(430, 20, 390, 200);
		layers.add(text, new Integer(10));
		parts.put(getSceneName() + "Dialogs", texts);
		
		enigme1 = new JTextArea(texts[1]);
		enigme1.setEditable(false);
		enigme1.setFont(new Font("Serif", Font.BOLD, 20));
		enigme1.setForeground(Color.white);
		enigme1.setBackground(new Color(0, 0, 255, 155));
		parts.put(getSceneName() + "TA", enigme1);
		enigme1.setBounds(430, 20, 390, 140);
		layers.add(enigme1, new Integer(10));
		parts.put(getSceneName() + "Dialogs", texts);
		enigme1.setVisible(false);
		enigme1.setEnabled(false);
		
		
		enigme2 = new JTextArea(texts[2]);
		enigme2.setEditable(false);
		enigme2.setFont(new Font("Serif", Font.BOLD, 20));
		enigme2.setForeground(Color.white);
		enigme2.setBackground(new Color(0, 0, 255, 155));
		parts.put(getSceneName() + "TA", enigme2);
		enigme2.setBounds(430, 20, 390, 140);
		layers.add(enigme2, new Integer(10));
		parts.put(getSceneName() + "Dialogs", texts);
		enigme2.setVisible(false);
		enigme2.setEnabled(false);
		
		
		text2 = new JTextArea(texts[3]);
		text2.setEditable(false);
		text2.setFont(new Font("Serif", Font.BOLD, 20));
		text2.setForeground(Color.white);
		text2.setBackground(new Color(0, 0, 255, 155));
		parts.put(getSceneName() + "TA", text2);
		text2.setBounds(430, 20, 390, 100);
		layers.add(text2, new Integer(10));
		parts.put(getSceneName() + "Dialogs", texts);
		text2.setVisible(false);
		text2.setEnabled(false);
		
		sc2Texts.add(text);
		sc2Texts.add(enigme1);
		sc2Texts.add(enigme2);
		sc2Texts.add(text2);
		parts.put("Dialogs", sc2Texts);
		
		// Initialize JButton validate
			validate = new JButton("Continue...");
			parts.put(getSceneName() + "Validate", validate);
			validate.setBounds(720, 230, 100, 50);
			validate.setForeground(Color.white);
			validate.setBackground(Color.blue);
			validate.setEnabled(true);
			validate.setVisible(true);
			layers.add(validate, new Integer(10));
		
		// Initialize JTextField playerChoice
		playerChoice = new JTextField(" Your answer ... ", 25);
		parts.put(getSceneName() + "IF", playerChoice);
		playerChoice.setBounds(480, 170, 290, 50);
		layers.add(playerChoice, new Integer(10));
		playerChoice.setEnabled(false);
		playerChoice.setVisible(false);
		
		//sc2Texts.add(text);
		
		
		
		
		
	}
}


//Si cela cache, ce n'est que pour mieux révéler.
//Cela bloque autant que cela permet de passer.
//La réponse est dans la question.
//Que suis-je ?


// A flying dragon look for it.
// A roaring dragon holds it in their talons.
// A sleeping dragon imagine it in their dreams.
// But it is there, Come and steal it from me, if you dare...

//Un dragon qui vole le cherche des yeux 
//Un dragon qui rugit le tiens entre ses serres 
//Un dragon qui sommeille l'imagine dans ces songes 
//Mais il est là, à côté si tu oses. Viens donc me le dérober.


//Je suis si fragile que lorsque l'on prononce mon nom, je meurs.









