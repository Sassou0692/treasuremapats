package org.TreasureMap.View;

import java.awt.Color;	//
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;	//

import java.util.ArrayList;

public class ScreenGrid extends SquareGrid {

	// // // // // ATTRIBUTES
	private JPanel[][] cells;
	private MouseListener listener;
	
	// // // // // CONSTRUCTORS
	// Classic screen
	public ScreenGrid(int nbRows, int nbCols, int squareSize)
	{
		// Parent constructor
		super(nbRows, nbCols, squareSize);
		// Initialize the accessible attributes
		interactable = false;
		exclusive = false;
		// Organize the layout and generate the cells
		cells = new JPanel[nbRows][nbCols];
		// Set the horizontal and vertical groups
		squarer();
	}

	// // // // // GETTERS
	public JPanel[][] getAllCells()
	{ return cells; }
	public JPanel getCell(int x, int y)
	{
		JPanel panel = null;
		try
		{ panel = cells[x][y]; }
		catch (ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Failure of retrieving cells : Out of array indexes.");
			System.err.println(e);
			e.printStackTrace();
		}
		return panel;
	}
	public JPanel[] getRow(int x)
	{
		JPanel[] panels = null;
		try
		{
			panels = new JPanel[nbCols];
			System.arraycopy(cells[x], 0, panels, 0, nbCols);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Failure of retrieving row : Out of array index.");
			System.err.println(e);
			e.printStackTrace();
		}
		return panels;
	}
	public JPanel[] getCol(int y)
	{
		JPanel[] panels = null;
		try
		{
			panels = new JPanel[nbRows];
			for (int i = 0; i < nbRows; i = i + 1)
			{ panels[i] = cells[i][y]; }
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			System.out.println("Failure of retrieving column : Out of array index.");
			System.err.println(e);
			e.printStackTrace();
		}
		return panels;
	}
	
	// // // // // SETTERS
	public void setListener(int mode)
	{
		listener = new MouseListener() 
		{
			public void mousePressed(MouseEvent e) { }
			public void mouseReleased(MouseEvent e) { }
			public void mouseEntered(MouseEvent e) { }
			public void mouseExited(MouseEvent e) { }
			public void mouseClicked(MouseEvent e)
			{ activeAdder((JPanel) e.getComponent()); }
		};
	}
	public void setListener(MouseListener listener)
	{ this.listener = listener; }
	public void interactable(boolean isInteractable)
	{ interactable = isInteractable; }

	
	public void exclusive(boolean isExclusive)
	{
		if (isExclusive)
		{ interactable = isExclusive; }
		exclusive = isExclusive;
	}
	
	// // // // // METHODS
	// Method override to instantiate and add to the ParallelGroups JPanels cells
	@Override
	protected GroupLayout.ParallelGroup celler(GroupLayout.ParallelGroup grp, int idx1, int idx2)
	{
		if (cells[idx1][idx2] == null)
		{
			cells[idx1][idx2] = new JPanel();
			cells[idx1][idx2].add(new JLabel(idx1 + ":" + idx2));	//
			if ((idx1 + idx2) % 2 == 0)								//
			{ cells[idx1][idx2].setBackground(Color.BLACK); }		//
			else													//
			{ cells[idx1][idx2].setBackground(Color.RED); }			//
		}
		grp.addComponent(cells[idx1][idx2], (size / 2), size, (size * 2));
		return grp;
	}
	
	// Methods to control the Listener activities :
	// Start the Listeners and set interactable true if interactable == false && listener != null
	public void startListening()
	{
		// Initialize actives
		actives = new ArrayList<JPanel>();
		// Activate the Listeners
		if (!interactable && listener != null)
		{
			for (JPanel[] row : cells)
			{
				for (JPanel cell : row)
				{ cell.addMouseListener(listener); }
			}
		}
		// Reassign interactable
		interactable = true;
	}
	// Update listener (remove the previous listener)
	public void switchListening(MouseListener listener)
	{
		// Activate the Listeners
		for (JPanel[] row : cells)
		{
			for (JPanel cell : row)
			{
				if (this.listener != null)
				{ cell.removeMouseListener(this.listener); }
				if (interactable)
				{ cell.addMouseListener(listener); }
			}
		}
		// Reassign listener
		this.listener = listener;
	}
	// End the Listeners and set interactable false if interactable == true && listener != null
	public void endListening()
	{
		// Deactivate the Listeners
		if (interactable && listener != null)
		{
			for (JPanel[] row : cells)
			{
				for (JPanel cell : row)
				{ cell.removeMouseListener(listener); }
			}
		}
		// Reassign interactable
		interactable = false;
	}
	// Clear the listener, remove the Listeners and set interactable false
	public void clearListening()
	{
		// Initialize actives
		actives = null;
		// Deactivate the Listeners
		if (listener != null)
		{
			for (JPanel[] row : cells)
			{
				for (JPanel cell : row)
				{ cell.removeMouseListener(listener); }
			}
		}
		// Reassign listener
		listener = null;
		// Reassign interactable
		interactable = false;
	}
	
	// Method to process the interactions :
	// Method to add a cell to the active cells : Non-exclusive
	private void activeAdder(JPanel cell)
	{ System.out.println(actives.size()); actives.add(cell); System.out.println(actives.size()); }
	// Method to change the active cell : Exclusive
	
}
