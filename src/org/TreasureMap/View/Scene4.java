package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Font;
// Imports Java Utilities
import java.util.HashMap;
import java.awt.color.*;
import java.awt.Font;

class Scene4 extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;
	//private JTextArea reward;
	private String[] texts;
	private JButton next;
	
	// // // // // CONSTRUCTORS
	Scene4(int width, int height) {
		// Scene constructor
		super("FourthScene", "findkey", width, height);

		// Initialize JTextArea text
		texts = new String[] {"    The key is here, are you able to find it ?  !\n " ,
							  "    You find the key! What a good boy!!"};
		parts.put("texts", texts);
		text = new JTextArea(texts[0]);
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setBackground(Color.BLACK);
		text.setForeground(Color.white);
		parts.put("Text", text);
		text.setBounds(230, 700, 400, 50);
		layers.add(text, new Integer(10));
		
//		reward = new JTextArea(texts[1]);
//		reward.setEditable(false);
//		reward.setFont(new Font("Serif", Font.BOLD, 20));
//		reward.setBackground(Color.orange);
//		parts.put("reward", reward);
//		reward.setBounds(230, 100, 450, 50);
//		layers.add(reward, new Integer(10));
//		reward.setVisible(false);
//		
		// Initialize JButton next
		next = new JButton();
		//next.setContentAreaFilled(false);
		parts.put(getSceneName() + "Validate", next);
		next.setContentAreaFilled(false);
		next.setBorderPainted(false);
		next.setBounds(250, 520, 80, 80);
		next.setVisible(true);
		next.setEnabled(true);
		layers.add(next, new Integer(10));

	}
	public JTextArea getLabel()
	{
		return text;
	}
	
}