package org.TreasureMap.View;

// Package imports
import org.TreasureMap.Controller.Controllers;
// Imports Swing
import javax.swing.JPanel;

public class Views {

	// Instance
	private static Views INSTANCE = null;
	// JFrame main window
	private Window window;
	// Different scenes
	private LaunchingScreen launch;
	private Map map;
	private Goals goals;
	private Scene1 first;
	private Scene2 second;
	private Scene3 third;
	private Scene4 fourth;
	private Scene5 fifth;
	private Scene6 sixth;
	//private ClosingScreen close;
	
	private Views()
	{
		// Instantiate Window
		window = new Window();
		// Initialize the scene content and size
		JPanel sceneContent = window.getSceneContent();
		int[] sceneSize = new int[] { sceneContent.getWidth(), sceneContent.getHeight() };
		// Instantiate LaunchScreen and update Window parts
		launch = new LaunchingScreen(sceneSize[0], sceneSize[1]);
		window.scenePutter(launch);
		// Instantiate Map and update Window parts
		map = new Map(sceneSize[0], sceneSize[1]);
		window.scenePutter(map);
		// Instantiate Goals and update Window parts
		goals = new Goals(sceneSize[0], sceneSize[1]);
		window.scenePutter(goals);
		// Instantiate Scene1 and update Window parts
		first = new Scene1(sceneSize[0], sceneSize[1]);
		window.scenePutter(first);
		// Instantiate Scene2 and update Window parts
		second = new Scene2(sceneSize[0], sceneSize[1]);
		window.scenePutter(second);
		// Instantiate Scene3 and update Window parts
		third = new Scene3(sceneSize[0], sceneSize[1]);
		window.scenePutter(third);
		// Instantiate Scene4 and update Window parts
		fourth = new Scene4(sceneSize[0], sceneSize[1]);
		window.scenePutter(fourth);
		// Instantiate Scene5 and update Window parts
		fifth = new Scene5(sceneSize[0], sceneSize[1]);
		window.scenePutter(fifth);
		// Instantiate Scene6 and update Window parts
		sixth = new Scene6(sceneSize[0], sceneSize[1]);
		window.scenePutter(sixth);

		
		/*
		launch = new LaunchingScene();
		first = new Scene1();
		second = new Scene2();
		third = new Scene3();
		*/
		
		//Controllers.getInstance();
	}
	
	public static Views getInstance()
	{
		if (INSTANCE == null)
		{ INSTANCE = new Views(); }
		return INSTANCE;
	}
	
	public Window getWindow()
	{ return window; }

	public Scene getScene(int scnIdx)
	{
		Scene scn = null;
		
		if (scnIdx == 0)
		{ scn = launch; }
		if (scnIdx == 1)
		{ scn = first; }
		if (scnIdx == 2)
		{ scn = second; }
		if (scnIdx == 3)
		{ scn = third; }
		if (scnIdx == 4)
		{ scn = fourth; }
		if (scnIdx == 5)
		{ scn = fifth; }
		if (scnIdx == 6)
		{ scn = sixth; }
		
		
		if (scnIdx == -1)
		{ scn = map; }
		if (scnIdx == -10)
		{ scn = goals; }
		//if (scnIdx == -100)
		//{ scn = close; }
		return scn;
	}
	public Scene getScene(String scnName)
	{
		Scene scn = null;
		int i = 0;
		while (i < window.getScenes().length && scn == null)
		{
			if (scnName.equals(window.getScenes()[i].getSceneName()))
			{ scn = window.getScenes()[i]; }
			i = i + 1;
		}
		return scn;
	}
	
	public static void launch()
	{
		INSTANCE = new Views();
		Controllers.getInstance();
	}
	
}
