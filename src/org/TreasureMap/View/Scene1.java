package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Font;
// Imports Java Utilities
import java.util.HashMap;
import java.awt.color.*;
import java.awt.Font;

class Scene1 extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;

	private JButton next;
	
	// // // // // CONSTRUCTORS
	Scene1(int width, int height) {
		// Scene constructor
		super("FirstScene", "cascadedoor", width, height);

		// Initialize JTextArea text
		text = new JTextArea();
		text = new JTextArea(
				"                What a beautiful waterfall !\n "
						+ "    Looks like there is a way through it ?!\n"
						+ "   If only i had something to help me through ?\n");
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setBackground(Color.orange);
		parts.put("Text", text);
		text.setBounds(230, 100, 400, 100);
		layers.add(text, new Integer(10));
		// Initialize JButton next
		next = new JButton("Go through");
		next.setBackground(Color.orange);
		parts.put(getSceneName() + "Validate", next);
		next.setBounds(350, 200, 160, 50);
		layers.add(next, new Integer(10));
		
	}

}
