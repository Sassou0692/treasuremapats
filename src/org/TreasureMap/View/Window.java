package org.TreasureMap.View;

// Imports Swing
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
// Imports AWT
import java.awt.CardLayout;
// Imports Java Utilities
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Window extends JFrame {

	// // // // // ATTRIBUTES
	// Window attributes
	private String name;
	private JFrame frame;
	private JPanel content;
	private GroupLayout layout;
	private JButton leftButton;
	private JButton middleButton;
	private JButton rightButton;
	// Scene attributes
	public JPanel scenePanel;
	private Scene currentScene;
	private Scene[] scenes;
	private CardLayout sceneLayout;
	// Pages essential components
	private HashMap<String, Object> windParts;
	
	// // // // // CONSTRUCTORS
	Window()
	{
		// JFrame constructor
		super("Pi-rat Island");
		// Assign name
		name = "Pi-rat Island";
		// Assign frame
		frame = this;
		// Resize the JFrame
		setSize(1000, 1000);
		// Initialize the main JPanel
		content = new JPanel();
		// Initialize the main layout
		layout = new GroupLayout(content);
		content.setLayout(layout);
		// Initialize the scene JPanel
		scenePanel = new JPanel();
		scenePanel.setSize(800, 800);
		// Initialize the scene CardLayout
		sceneLayout = new CardLayout();
		// Set the scene layout
		scenePanel.setLayout(sceneLayout);
		// Initialize the JButtons
		leftButton = new JButton("Map");
		middleButton = new JButton("Goals");
		rightButton = new JButton("ARRGH!");
		// Initialize the window UI parts
		hashMapper();
		// // Initialize the scenes
		// sceneAdder();
		// Set the Window layout
		windowLayoutSetler();
		// Finish the adjustment of the JFrame
		setDefaultCloseOperation(3);
		setContentPane(content);
		setVisible(true);
	}
	
	// // // // // GETTERS
	public JPanel getSceneContent()
	{ return scenePanel;  }
	public HashMap<String, Object> getParts()
	{ return windParts; }
	//public Scene getCurrentScene()
	//{ return currentScene; }
	public Scene[] getScenes()
	{ return scenes; }
	
	// Method to add a scene
	public void scenePutter(Scene scn)
	{
		if (currentScene == null)
		{ currentScene = scn; }
		if (scenes == null)
		{ scenes = new Scene[0]; }
		windParts.put(scn.getSceneName(), scn);
		List<Scene> tempScenes = new ArrayList<Scene>(Arrays.asList(scenes));
		tempScenes.add(scn);
		scenes = tempScenes.toArray(new Scene[0]);
		windParts.put("Scenes", scenes);
		scenePanel.add(scn, scn.getSceneName());
	}
	
	/*
	// Method to instantiate and add the different pages
	private void sceneAdder()
	{
		// Initialize scenes
		scenes = new Scene[0];
		// Initialize the Launch screen
		Scene tempScn = new LaunchingScreen(
				scenePanel.getWidth(), 
				scenePanel.getHeight()
				);
		scenePutter(tempScn);
		// Initialize the Map
		tempScn = new Map(
				scenePanel.getWidth(), 
				scenePanel.getHeight()
				);
		scenePutter(tempScn);
		// Initialize the First Scene
		tempScn = new Scene1(
				scenePanel.getWidth(), 
				scenePanel.getHeight()
				);
		scenePutter(tempScn);
		
		windParts.put("Scenes", scenes);
	}
	*/
	
	// Methods to fill parts
	private void hashMapper()
	{
		windParts = new HashMap<String, Object>();
		windParts.put("Name", name);
		windParts.put("Frame", frame);
		windParts.put("Content", content);
		windParts.put("ScenePanel", scenePanel);
		windParts.put("SceneLayout", sceneLayout);
		windParts.put("LeftButton", leftButton);
		windParts.put("MiddleButton", middleButton);
		windParts.put("RightButton", rightButton);
	}
	
	// Method to set the window layout
	private void windowLayoutSetler()
	{
		// Set the Window layout
		// Horizontal Layout
		GroupLayout.SequentialGroup horiz = layout.createSequentialGroup()
				.addGap(25, 50, 550)
				.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addComponent(scenePanel, 750, 800, 850)
						.addGroup(layout.createSequentialGroup()
								.addComponent(leftButton, 50, 75, 100)
								.addGap(275, 283, 300)
								.addComponent(middleButton, 50, 75, 100)
								.addGap(275, 283, 300)
								.addComponent(rightButton, 50, 75, 100)))
				.addGap(25, 50, 550);
		layout.setHorizontalGroup(horiz);
		// Vertical Layout
		GroupLayout.SequentialGroup vert = layout.createSequentialGroup()
				.addGap(25, 50, 75)
				.addComponent(scenePanel, 750, 800, 850)
				.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(leftButton, 25, 50, 75)
						.addComponent(middleButton, 25, 50, 75)
						.addComponent(rightButton, 25, 50, 75))
				.addGap(25, 50, 75);
		layout.setVerticalGroup(vert);
	}
	
}
