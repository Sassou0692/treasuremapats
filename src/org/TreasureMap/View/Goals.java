package org.TreasureMap.View;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Goals extends Scene {

	private JLabel title;
	private JTextArea[] texts;
	
	Goals(int width, int height) {
		// Scene constructor
		super("Goals", "GoalScroll", width, height);
		// Initialize JLabel
		title = new JLabel("Goals");
		title.setFont(new Font("Serif", Font.BOLD, 50));
		title.setBackground(new Color(55, 55, 55, 0));
		title.setForeground(new Color(85, 40, 85, 155));
		title.setBounds(350, 100, 250, 100);
		title.setEnabled(true);
		title.setVisible(true);
		parts.put(getSceneName() + "Title", title);
		layers.add(title, new Integer(10));
		// Initialize JTextAreas
		texts = new JTextArea[7];
		int heightMob = 250;
		texts = new JTextArea[7];
		for (int i = 0; i < texts.length; i = i + 1)
		{
			texts[i] = new JTextArea();
			if (i == 0)
			{ texts[i].setText("Give your name"); }
			else if (i == 1)
			{ texts[i].setText("Find the way to the TREASURE"); }
			else if (i == 2)
			{ texts[i].setText("Find a way to access the TREASURE"); }
			else if (i == 3)
			{ texts[i].setText("Find a way to acquire the TREASURE"); }
			else if (i == 4)
			{ texts[i].setText("Unlock the door with a KEY"); }
			else if (i == 5)
			{ texts[i].setText("Unlock the door by resolving the RIDDLE"); }
			else if (i == 6)
			{ texts[i].setText("Get the TREASURE"); }
			texts[i].setFont(new Font("Serif", Font.BOLD, 30));
			texts[i].setBackground(new Color(55, 55, 55, 0));
			texts[i].setForeground(new Color(85, 40, 85, 155));
			texts[i].setBounds(150, heightMob, 600, 50);
			texts[i].setEnabled(false);
			texts[i].setVisible(false);
			layers.add(texts[i], new Integer(10));
			heightMob = heightMob + 50;
		}
		parts.put(getSceneName() + "TAs", texts);
		
		/*
		first = new JButton("X");
		first.setFont(new Font("Serif", Font.BOLD, 50));
		first.setBackground(new Color(55, 55, 55, 0));
		first.setForeground(new Color(255, 0, 0, 255));
		first.setBorderPainted(true);
		first.setContentAreaFilled(false);
		first.setFocusPainted(true);
		first.setBounds(485, 375, 75, 75);
		first.setEnabled(false);
		first.setVisible(false);
		parts.put(getSceneName() + "TA", first);
		layers.add(first, new Integer(10));
		*/
	}
	
}
