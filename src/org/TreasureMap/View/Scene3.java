package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Font;
// Imports Java Utilities
import java.util.HashMap;
import java.awt.color.*;
import java.awt.Font;

class Scene3 extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;

	private JButton next;
	
	// // // // // CONSTRUCTORS
	Scene3(int width, int height) {
		// Scene constructor
		super("ThirdScene", "island", width, height);

		// Initialize JTextArea text
		text = new JTextArea(
				"       Ohhh... i'm on the island... \n"
				+ "      There is nothing here !\n"
				+ "     I Have to search another way.... ");
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setBackground(new Color(0, 0, 255, 155));
		text.setForeground(Color.white);
		parts.put("Text", text);
		text.setBounds(230, 100, 300, 100);
		layers.add(text, new Integer(10));
		
		// Initialize JButton next
				next = new JButton("Continue...");
				parts.put(getSceneName() + "Validate", next);
				next.setBackground(Color.blue);
				next.setForeground(Color.white);
				next.setBounds(300, 200, 120, 50);
				layers.add(next, new Integer(10));
				
		
		
	}

}
