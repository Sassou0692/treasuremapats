package org.TreasureMap.View;

import java.awt.Color;
import java.awt.Font;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Map extends Scene {

	// // // // // ATTRIBUTES
	private JButton first;
	private JButton second;
	private JButton third;
	private JButton fourth;
	
	// // // // // CONSTRUCTORS
	public Map(int width, int height) {
		// Scene constructor
		super("Map", "Treasuremap", width, height);
		// Initialize JButton validate
		first = new JButton("X");
		first.setFont(new Font("Serif", Font.BOLD, 50));
		first.setBackground(new Color(55, 55, 55, 0));
		first.setForeground(new Color(255, 0, 0, 255));
		first.setBorderPainted(true);
		first.setContentAreaFilled(false);
		first.setFocusPainted(true);
		first.setBounds(485, 375, 75, 75);
		first.setEnabled(false);
		first.setVisible(false);
		parts.put(getSceneName() + "Loc:1", first);
		layers.add(first, new Integer(10));
		// Initialize JButton validate
		second = new JButton("X");
		second.setFont(new Font("Serif", Font.BOLD, 50));
		second.setBackground(new Color(55, 55, 55, 0));
		second.setForeground(new Color(255, 0, 0, 255));
		second.setBorderPainted(true);
		second.setContentAreaFilled(false);
		second.setFocusPainted(true);
		second.setBounds(575, 575, 75, 75);
		second.setEnabled(false);
		second.setVisible(false);
		parts.put(getSceneName() + "Loc:2", second);
		layers.add(second, new Integer(10));
		// Initialize JButton validate
		third = new JButton("X");
		third.setFont(new Font("Serif", Font.BOLD, 50));
		third.setBackground(new Color(55, 55, 55, 0));
		third.setForeground(new Color(255, 0, 0, 255));
		third.setBorderPainted(true);
		third.setContentAreaFilled(false);
		third.setFocusPainted(true);
		third.setBounds(655, 285, 75, 75);
		third.setEnabled(false);
		third.setVisible(false);
		parts.put(getSceneName() + "Loc:3", third);
		layers.add(third, new Integer(10));
		// Initialize JButton validate
		fourth = new JButton("X");
		fourth.setFont(new Font("Serif", Font.BOLD, 50));
		fourth.setBackground(new Color(55, 55, 55, 0));
		fourth.setForeground(new Color(255, 0, 0, 255));
		fourth.setBorderPainted(true);
		fourth.setContentAreaFilled(false);
		fourth.setFocusPainted(true);
		fourth.setBounds(150, 375, 75, 75);
		fourth.setEnabled(false);
		fourth.setVisible(false);
		parts.put(getSceneName() + "Loc:4", fourth);
		layers.add(fourth, new Integer(10));
	}
	
}
