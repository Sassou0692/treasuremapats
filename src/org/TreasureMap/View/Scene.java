package org.TreasureMap.View;

// Imports IO
import java.io.File;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
// Imports Image
import java.awt.Image;
import java.awt.image.BufferedImage;
// Imports Swing
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
// Imports AWT
import java.awt.Dimension;
// Imports Java Utilities
import java.util.HashMap;

public abstract class Scene extends JPanel {

	// // // // // ATTRIBUTES
	// Scene ID
	private String sceneName;
	// Background Image
	private String sceneBGPath;
	private URL imgURL;
	private Image img;
	private ImageIcon sceneImg;
	private JLabel bg;
	// Layered panels
	protected JLayeredPane layers;
	// Necessary UI parts message
	protected HashMap<String, Object> parts;

	// // // // // CONSTRUCTORS
	Scene(String name, String fileName, int width, int height)
	{
		// JPanel constructor
		super();
		// Initialize ID
		sceneName = name;
		// Initialize parts
		parts = new HashMap<String, Object>();
		parts.put("SceneName", sceneName);
		// Initialize Layered panels
		layers = new JLayeredPane();
		layers.setPreferredSize(new Dimension(width, height));
		// Initialize Background Image
		sceneBGPath = "";
		imgURL = null;
		img = null;
		sceneImg = null;
		imageReader(fileName);
		imageTransformer(width, height);
		backgroundInitilizer(width, height);
		
		/*
		JLabel test1 = new JLabel("Test");
		test1.setBounds(50, 50, 100, 100);
		layers.add(test1, new Integer(50));
		JLabel test2 = new JLabel("Test !!!");
		test2.setBounds(600, 600, 100, 100);
		layers.add(test2, new Integer(150));
		*/

		add(layers);
		
	}

	// // // // // GETTERS
	public String getSceneName()
	{ return sceneName; }
	public HashMap<String, Object> getParts()
	{ return parts; }
	// // // // // SETTERS

	// // // // // METHODS
	
	// Method to encapsulate and auto-close the Image InputStream
	private BufferedImage autoCloseRead(URL url)
	{
		try (InputStream stream = url.openStream())
		{ return ImageIO.read(stream); }
		catch (IOException e)
		{
			System.out.println("Failure of InputStream");
			System.err.println(e);
			e.printStackTrace();
			return null;
		}
	}
	// Method to load from the Resources the Image
	private void imageReader(String fileName)
	{
		if (fileName.equals("None"))
		{ return; }
		try
		{
			sceneBGPath = Paths.get("./src/org/TreasureMap/View/Images/" + fileName + ".jpg")
					.toAbsolutePath().normalize().toString();
			imgURL = new File(sceneBGPath).toURI().toURL();
			img = autoCloseRead(imgURL);
		}
		catch (IOException e)
		{
			System.out.println("Failure of ImageIO reading (Path String) : " + sceneBGPath);
			System.out.println("Failure of ImageIO reading (URL) : " + imgURL.toString());
			System.out.println("Failure of ImageIO reading (Image) : " + img.toString());
			System.err.println(e);
			e.printStackTrace();
		}
	}
	// Method to resize and cast the Image
	private void imageTransformer(int width, int height)
	{
		if (img == null)
		{ return; }
		img = img.getScaledInstance(width, height, Image.SCALE_DEFAULT);
		sceneImg = new ImageIcon(img);
	}
	// Method to initialize the Background
	private void backgroundInitilizer(int width, int height)
	{
		if (sceneImg == null)
		{ return; }
		bg = new JLabel();
		bg.setIcon(sceneImg);
		bg.setBounds(0, 0, width, height);
		layers.add(bg, new Integer(0));
	}
	
}
