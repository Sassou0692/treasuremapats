package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Font;
// Imports Java Utilities
import java.util.HashMap;
import java.awt.color.*;
import java.awt.Font;

class Scene6 extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;

	private JButton next;
	
	// // // // // CONSTRUCTORS
	Scene6(int width, int height) {
		// Scene constructor
		super("SixthScene", "treasure", width, height);

		// Initialize JTextArea text
		text = new JTextArea(
				" I'VE FOUND IT !!!!\n ");
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 20));
		text.setBackground(Color.orange);
		parts.put("Text", text);
		text.setBounds(230, 700, 190, 40);
		layers.add(text, new Integer(10));
		// Initialize JButton next
		next = new JButton("Escape");
		parts.put(getSceneName() + "Validate", next);
		next.setBounds(250, 250, 300, 50);
		layers.add(next, new Integer(10));
		next.setVisible(false);
		
	}

}
