package org.TreasureMap.View;

// Imports Swing
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
// Imports AWT
import java.awt.Font;
// Imports Java Utilities
//import java.util.HashMap;

class LaunchingScreen extends Scene {

	// // // // // ATTRIBUTES
	private JTextArea text;
	private JTextField playerName;
	private JButton validate;

	// // // // // CONSTRUCTORS
	LaunchingScreen(int width, int height) {
		// Scene constructor
		super("LaunchScreen", "LaunchBackground", width, height);
		
		// Initialize JTextArea text
		text = new JTextArea();
		text = new JTextArea(
				"	Hello !\n\n" + 
				"What a blue pirate like you do on my ship ?!\n" + 
				"	What's your name ?\n");
		text.setEditable(false);
		text.setFont(new Font("Serif", Font.BOLD, 16));
		parts.put("Text", text);
		text.setBounds(250, 100, 300, 100);
		layers.add(text, new Integer(10));
		
		// Initialize JTextField playerName
		playerName = new JTextField("Name ?", 25);
		parts.put(getSceneName() + "IF", playerName);
		playerName.setBounds(250, 200, 300, 50);
		layers.add(playerName, new Integer(10));
		
		// Initialize JButton validate
		validate = new JButton("Play ?");
		parts.put(getSceneName() + "Validate", validate);
		validate.setBounds(250, 250, 300, 50);
		layers.add(validate, new Integer(10));
	}

}
