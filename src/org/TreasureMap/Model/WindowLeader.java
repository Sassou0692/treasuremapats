package org.TreasureMap.Model;

public class WindowLeader extends Notes {

	// // // // // ATTRIBUTES
	private String[] reachedGoals;
	private String[] currentGoals;
	private String[] accessLocations;
	
	// // // // // CONSTRUCTORS
	WindowLeader()
	{
		
	}
	// // // // // GETTERS
	
	// // // // // SETTERS
	
	// // // // // METHODS
	
	// Method to update the accessible locations
	private void locationUpdate(String[] locations)
	{
		accessLocations = locations;
	}
	
	// Methods to control the View scene changes :
	// Method to control the Map screen printing
	public String mapPrinter(String sceneName)
	{
		return "ForDev";
	}
	// Method to control the Goal screen printing
	public String goalPrinter(String sceneName)
	{
		return "ForDev";
	}

}
