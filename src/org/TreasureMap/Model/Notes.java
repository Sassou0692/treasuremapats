package org.TreasureMap.Model;

// Imports Java utilities
import java.util.HashMap;
/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
*/

public class Notes {

	// Determinate the collection of accessible scenes in function of the progression (rank)
	private static final String[][] scenes = new String[][] 
			{
		new String[] { "LaunchScreen" }, 
		new String[] { "FirstScene" }, 
		new String[] { "FirstScene", "SecondScene" },
		new String[] { "ThirdScene", "SecondScene", "FirstScene" },
		new String[] { "FirstScene", "SecondScene", "ThirdScene", "FourthScene" },
		new String[] { "FifthScene","SixthScene" }, 
		new String[] { "SixthScene" }, 
		new String[] { "ClosingScreen" }
			};

	// Determinate the collection of accessible scenes in function of the progression (rank)
	private static final HashMap<String, String> scenesToSpots = createScenesToSpots();
			
	// Determinate the collection of goals in function of the progression (rank)
	private static final String[][] goals = new String[][] 
			{
		new String[] 
			{ "Give your name" }, 
		new String[] 
			{ 
				"Find the way to the TREASURE", 
				"Find a way to access the TREASURE", 
				"Find a way to acquire the TREASURE" 
			}, 
		new String[] 
			{ 
				"Find the way to the TREASURE", 
				"Find a way to access the TREASURE", 
				"Find a way to acquire the TREASURE", 
				"Unlock the door with a KEY"
			},
		new String[] 
			{
				"Find a way to access the TREASURE", 
				"Find a way to acquire the TREASURE", 
				"Unlock the door by resolving the RIDDLE"
			}, 
		new String[] 
			{ "Find a way to acquire the TREASURE", "Get the TREASURE" }
		// Others ?
		};
	
	public static String[][] getScenes()
	{ return scenes; }
	public static String[][] getGoals()
	{ return goals; }
	public static String getSpot(String scene)
	{
		String newSpot = "";
		if (scenesToSpots.containsKey(scene))
		{ newSpot = scenesToSpots.get(scene); }
		return newSpot;
	}
		
	private static HashMap<String, String> createScenesToSpots()
	{
		HashMap<String, String> newMap = new HashMap<String, String>();
		newMap.put("FirstScene", "Loc:1");
		newMap.put("SecondScene", "Loc:2");
		newMap.put("ThirdScene", "Loc:3");
		newMap.put("FourthScene", "Loc:4");
		return newMap;
	}
		
}
