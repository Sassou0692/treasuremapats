package org.TreasureMap.Model;

public class Models {

	private static Models INSTANCE = null;
	
	private WindowModel windModel;
	private LaunchModel launchModel;
	//private TiBiModel tibi;
	private Scene1Model sc1Model;
	private Scene2Model sc2Model;
	private Scene3Model sc3Model;
	
	private Models()
	{
		windModel = new WindowModel();
		launchModel = new LaunchModel();
		//tibi = new TiBiModel();
		sc1Model = new Scene1Model();
		sc2Model = new Scene2Model();
		sc3Model = new Scene3Model();
	}
	
	public static Models getInstance()
	{
		if (INSTANCE == null)
		{ INSTANCE = new Models(); }
		return INSTANCE;
	}
	
	public WindowModel getWindowModel()
	{ return windModel; }
	//public TiBiModel getTiBiModel()
	//{ return tibi; }
	public SceneModel getSceneModel(int sceneNbr)
	{
		SceneModel model = null;
		if (sceneNbr == 0)
		{ model = launchModel; }
		if (sceneNbr == 1)
		{ model = sc1Model; }
		if (sceneNbr == 2)
		{ model = sc2Model; }
		if (sceneNbr == 3)
		{ model = sc3Model; }
		return model;
	}
	
	public String getPlayerName()
	{ return launchModel.getPlayerName(); }
	public String getCurrentScene()
	{ return windModel.getCurrentScene(); }
	
}
