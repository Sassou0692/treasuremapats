package org.TreasureMap.Model;

import org.TreasureMap.Controller.Controllers;

public class WindowModel {

	// // // // // ATTRIBUTES
	private String playerName;
	private String[][] goals;
	private String[][] scenes;
	private int location;
	private int rank;
	private String currentScene;
	
	// // // // // CONSTRUCTORS
	WindowModel()
	{
		playerName = "";
		goals = Notes.getGoals();
		scenes = Notes.getScenes();
		location = 0;
		rank = 0;
		currentScene = scenes[rank][location];
	}
	
	// // // // // GETTERS
	public String getCurrentScene()
	{ return currentScene; }
	
	// // // // // SETTERS
	public void setPlayerName(String plName)
	{
		playerName = plName;
	}
	public void setCurrentScene(String scn)
	{ currentScene = scn; }
	
	// // // // // METHODS
	// Methods to control the View scene changes :
	// Method to control the Map screen printing
	public String[] mapUpdater()
	{
		if (rank == 0) { return null; }
		String[] newSpots = new String[scenes[rank].length];
		for (int i = 0; i < scenes[rank].length; i = i + 1)
		{ newSpots[i] = Notes.getSpot(scenes[rank][i]); }
		return newSpots;
	}
	public String[] goalsUpdater()
	{
		String[] newGoals = new String[goals[rank].length];
		System.arraycopy(goals[rank], 0, newGoals, 0, goals[rank].length);
		return newGoals;
	}
	// Method to control the Map screen printing
	public String mapPrinter()
	{
		if (rank == 0)
		{ return ""; }
		String newScene = "";
		if (Controllers.getInstance().getWindowController().getMapDisplay())
		{
			if (Controllers.getInstance().getWindowController().getMapExit())
			{ newScene = currentScene; }
			else
			{ newScene = "Map"; }
		}
		else
		{ newScene = "Map"; }
		return newScene;
	}
	public String mapForcedPrinter()
	{
		Controllers.getInstance().getWindowController().setMapExit(false);
		return "Map";
	}
	// Method to control the Goal screen printing
	public String goalPrinter()
	{
		String newScene = "";
		if (Controllers.getInstance().getWindowController().getGoalsDisplay())
		{ newScene = currentScene; }
		else
		{ newScene = "Goals"; }
		return newScene;
	}

	// Method to update the scene
	private String sceneUpdate(String scn)
	{
		currentScene = scn;
		return scn;
	}
	// Method to change the current scene in function of the newly accessible scenes
	public String progression(boolean next)
	{
		String sceneName = currentScene;
		if (next)
		{
			rank = rank + 1;
			if (scenes[rank].length <= location)
			{ location = 0; }
			sceneName = scenes[rank][location];
		}
		return sceneUpdate(sceneName);
	}
	// Method to change the current scene in function of the accessible scenes
	public String move(String next)
	{
		String sceneName = scenes[rank][location];
		int i = 0;
		while (i < scenes[rank].length && sceneName.equals(scenes[rank][location]))
		{
			if (scenes[rank][i].equals(next))
			{
				sceneName = next;
				location = i;
			}
			i = i + 1;
		}
		Controllers.getInstance().getWindowController().setMapExit(true);
		return sceneUpdate(sceneName);
	}

}
