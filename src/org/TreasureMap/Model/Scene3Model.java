package org.TreasureMap.Model;

public class Scene3Model extends SceneModel {

	// // // // // ATTRIBUTES
	private boolean visited;
	private boolean key;
		
	// // // // // CONSTRUCTORS
	Scene3Model()
	{
		visited = false;
		key = false;
		done = false;
	}
	
	// // // // // METHODS
	// Parent method overrides
	@Override
	public boolean itemChecker()
	{
		if (!visited)
		{
			visited = true;
			return visited;
		}
		else if (key)
		{ done = true; }
		return key;
	}
	
}
