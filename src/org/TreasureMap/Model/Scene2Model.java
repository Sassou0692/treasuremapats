package org.TreasureMap.Model;

import java.util.regex.Pattern;

public class Scene2Model extends SceneModel {

	Scene2Model() { }
	
	@Override
	public boolean textChecker1(String text)
	{
		boolean out = false;
		if (Pattern.matches("^( )*TREASURE(S)*( )*$", text.toUpperCase()))
		{ out = true; }
		return out;
	}
	@Override
	public boolean textChecker2(String text)
	{
		boolean out = false;
		if (Pattern.matches("^( )*SILENCE(S)*( )*$", text.toUpperCase()))
		{ out = true; }
		return out;
	}
	
	// // // // // SETTERS
	@Override
	public void setItem(boolean got)
	{ Models.getInstance().getSceneModel(1).setItem(got); }
			
}
