package org.TreasureMap.Model;

public class Scene1Model extends SceneModel {

	// // // // // ATTRIBUTES
	private boolean visited;
	private boolean umbrella;
		
	// // // // // CONSTRUCTORS
	Scene1Model()
	{
		visited = false;
		umbrella = false;
		done = false; 
		
	}
	
	// // // // // SETTERS
	@Override
	public void setItem(boolean got)
	{ umbrella = got; }
	
	// // // // // METHODS
	// Parent method overrides
	@Override
	public boolean itemChecker()
	{
		if (!visited)
		{
			visited = true;
			return visited;
		}
		else if (umbrella)
		{ done = true; }
		return umbrella;
	}
	
}
