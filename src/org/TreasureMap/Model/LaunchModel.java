package org.TreasureMap.Model;

// Imports Java utilities
import java.util.regex.Pattern;

public class LaunchModel extends SceneModel {

	// // // // // ATTRIBUTES
	private String playerName;
	
	// // // // // CONSTRUCTORS
	LaunchModel()
	{ playerName = ""; }
	
	// // // // // GETTERS
	public String getPlayerName()
	{ return playerName; }
	
	// // // // // METHODS
	// Parent method overrides
	@Override
	public boolean textChecker1(String text)
	{
		boolean out = false;
		if (Pattern.matches("^[A-z0-9 \\-\\_]+$", text))
		{
			out = true;
			playerName = text;
		}
		return out;
	}
	@Override
	public boolean textChecker2(String text)
	{
		boolean out = false;
		if (Pattern.matches("^[A-z0-9 \\-\\_]+$", text))
		{
			out = true;
			playerName = text;
		}
		return out;
	}
	
}
