package org.TreasureMap.Controller;

// Package imports
import org.TreasureMap.Model.Models;
// Imports Swing
import javax.swing.JTextField;
import javax.swing.JButton;

public class LaunchController extends SceneController {

	// // // // // ATTRIBUTES
	private JTextField inputF;
	private JButton button;
	
	// // // // // CONSTRUCTORS
	LaunchController()
	{
		// SceneController constructor
		super();
		// Initialize the scene index and name
		sceneIdx = 0;
		sceneNameRetriever(sceneIdx);
		// Initialize the UI components and listeners
		inputF = inputFAssigner();
		button = buttonAssigner();
	}
	
	// // // // // METHODS
	// Parent method overrides
	@Override
	protected void reassignInputF(JTextField inputF)
	{
		if (inputFRetriever(inputF).equals(""))
		{ inputF.setText("Name ?"); }
	}
	@Override
	protected void validityCheck()
	{
		Controllers.getInstance().getWindowController().screenChanger(
				Models.getInstance().getWindowModel().progression(
						Models.getInstance().getSceneModel(sceneIdx).textChecker1(
								inputFRetriever(inputF))));
		Controllers.getInstance().getWindowController().forceMap();
	}
	
}
