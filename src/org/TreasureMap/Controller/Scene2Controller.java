package org.TreasureMap.Controller;

//Package imports
import org.TreasureMap.Model.Models;
import org.TreasureMap.Model.*;
//Package imports
import org.TreasureMap.View.Views;
// Imports Swing
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.util.ArrayList;

import javax.swing.JButton;

public class Scene2Controller extends SceneController {

	private JTextArea textA;
	private String[] texts;
	private JTextField inputF;
	private JButton button;
	private int dialogIdx;
	private int dialogMax;
	//private BiModel leader;
	
	Scene2Controller()
	{
		// SceneController constructor
		super();
		// Initialize the scene index and name
		sceneIdx = 2;
		sceneNameRetriever(sceneIdx);
		// Initialize the UI components and listeners
		textA = (JTextArea) Views.getInstance().getScene(sceneIdx).getParts().get(sceneName + textAKey);
		texts = (String[]) Views.getInstance().getScene(sceneIdx).getParts().get(sceneName + dialogsKey);
		//textAListening(textA);
		inputF = inputFAssigner();
		button = buttonAssigner();
		dialogIdx = 0;
		dialogMax = 3;
	}
	
	@Override
	protected void rewriteTextA(String text)
	{ textA.setText(text); }
	
	@Override
	protected void validityCheck()
	{
		ArrayList<JTextArea> dia = (ArrayList<JTextArea>) Views.getInstance().getScene(sceneIdx).getParts().get("Dialogs");

		if (dialogIdx < dialogMax)
		{
		if (dialogIdx == 0) 
		{
			
			dia.get(0).setVisible(false);
			dia.get(0).setEnabled(false);
			
			dia.get(1).setVisible(true);
			dia.get(1).setEnabled(true);

			inputF.setVisible(true);
			inputF.setEnabled(true);
			
			// Method to disable/enable JTextArea
			dialogIdx = dialogIdx + 1;
		}		
		
		if (dialogIdx == 1)
		{	
			boolean answer1 = Models.getInstance().getSceneModel(sceneIdx).textChecker1(inputFRetriever(inputF));
			if (answer1)
			{			
				dialogIdx = dialogIdx + 1;
				dia.get(1).setVisible(false);
				dia.get(1).setEnabled(false);
				
				dia.get(2).setVisible(true);
				dia.get(2).setEnabled(true);
			}
		}
		
		if (dialogIdx == 2)
		{	
			boolean answer2 = Models.getInstance().getSceneModel(sceneIdx).textChecker2(inputFRetriever(inputF));
			if (answer2)
			{			
				dialogIdx = dialogIdx + 1;
				
				dia.get(2).setVisible(false);
				dia.get(2).setEnabled(false);
				
				inputF.setVisible(false);
				inputF.setEnabled(false);
				
				dia.get(3).setVisible(true);
				dia.get(3).setEnabled(true);
				
				button.setVisible(false);
				
				Models.getInstance().getSceneModel(sceneIdx).setItem(true);
			}
		}
		
		
		if (dialogIdx == dialogMax)
		{
			/*
			// Method to check JTextField and change scene
			dia.get(2).setVisible(false);
			dia.get(2).setEnabled(false);
			
			inputF.setVisible(false);
			inputF.setEnabled(false);
			
			dia.get(3).setVisible(true);
			dia.get(3).setEnabled(true);
			
			button.setVisible(false);
			
			Models.getInstance().getSceneModel(sceneIdx).setItem(true);
			*/
			
		}
		
	}
		/*
		Controllers.getInstance().getWindowController().screenChanger(
				Models.getInstance().getWindowModel().progression(
						Models.getInstance().getSceneModel(sceneIdx).textChecker1(
								inputFRetriever(inputF))));
		*/
	
	}	
}
