package org.TreasureMap.Controller;

// Package imports
import org.TreasureMap.Model.Models;

public class Controllers {

	// Instance
	private static Controllers INSTANCE = null;
	// Window Controller
	private WindowController window;
	// Scene Controllers
	private LaunchController launch;
	private Scene1Controller sc1;
	private Scene2Controller sc2;
	private Scene3Controller sc3;
	private Scene4Controller sc4;
	private Scene5Controller sc5;
	private Scene6Controller sc6;
	
	private Controllers()
	{
		// Instantiate Window Controller
		window = new WindowController();
		// Instantiate Launch Controller
		launch = new LaunchController();
		// Instantiate Scene 1 Controller
		sc1 = new Scene1Controller();
		// Instantiate Scene 2 Controller
		sc2 = new Scene2Controller();
		// Instantiate Scene 3 Controller
		sc3 = new Scene3Controller();
		// Instantiate Scene 3 Controller
		sc4 = new Scene4Controller();
		// Instantiate Scene 3 Controller
		sc5 = new Scene5Controller();
		// Instantiate Scene 3 Controller
		sc6 = new Scene6Controller();
		
		Models.getInstance();
		
	}
	
	public static Controllers getInstance()
	{
		if (INSTANCE == null)
		{ INSTANCE = new Controllers(); }
		return INSTANCE;
	}
	
	public WindowController getWindowController()
	{ return window; }
	
}
