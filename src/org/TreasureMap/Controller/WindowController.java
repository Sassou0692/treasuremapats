package org.TreasureMap.Controller;

// Package imports
import org.TreasureMap.View.Views;
import org.TreasureMap.Model.Models;
// Imports Swing
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextArea;
// Imports AWT
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import java.awt.Color;
// Imports Java utilities
import java.util.HashMap;
import java.util.Arrays;

public class WindowController {
	
	// // // // // ATTRIBUTES
	// View UI parts container HashMap
	private HashMap<String, Object> uiParts;
	// View UI parts
	private JFrame frame;
	private JPanel scenePanel;
	private CardLayout sceneLayout;
	//private Scene currentScene;
	//private Scene[] scenes;
	private JButton leftButton;
	private JButton middleButton;
	private JButton rightButton;
	// Model behaviors WindowLeader
	//private WindowLeader leader;
	// Map behavior attributes
	private boolean displayedMap;
	private boolean exitableMap;
	private boolean displayedGoals;
	
	// // // // // CONSTRUCTORS
	WindowController()
	{
		HashMap<String, Object> parts = Views.getInstance().getWindow().getParts();
		
		// Initialize the Attributes from the HashMap data
		hashMapReader(parts);
		// Add the JButton Listeners
		listenerAdder();
		
		displayedMap = false;
		exitableMap = false;
		displayedGoals = false;
		
		// Initialize the Model WindowLeader leader
		//leader = Models.getInstance().getWindowLeader();
		
		/*
		for (Scene sc : scenes)
		{
			//System.out.println(sc.toString());
			if (sc.getID().equals("TextInButtonIn"))
			{ new TextInButtonIn(this, sc); }
			if (sc.getID().equals("ButtonIn"))
			{ new ButtonIn(this, sc); }
			if (sc.getID().equals("2"))
			{
				new SecondSceneController(this, sc);
			}
		}
		*/
	}
	
	// // // // // GETTERS
	public boolean getMapDisplay()
	{ return displayedMap; }
	public boolean getMapExit()
	{ return exitableMap; }
	public boolean getGoalsDisplay()
	{ return displayedGoals; }
	
	// // // // // SETTERS
	public void setMapExit(boolean exit)
	{ exitableMap = exit; }
	
	// // // // // METHODS
	// Methods to construct the instance :
	// Method to load the data from the HashMap
	private void hashMapReader(HashMap<String, Object> parts)
	{
		// Initialize the HashMap UI parts
		uiParts = parts;
		// Initialize the UI parts
		frame = (JFrame) uiParts.get("Frame");
		scenePanel = (JPanel) uiParts.get("ScenePanel");
		sceneLayout = (CardLayout) uiParts.get("SceneLayout");
		//currentScene = (Scene) uiParts.get("CurrentScene");
		leftButton = (JButton) uiParts.get("LeftButton");
		middleButton = (JButton) uiParts.get("MiddleButton");
		rightButton = (JButton) uiParts.get("RightButton");
		//scenes = (Scene[]) uiParts.get("Scenes");
	}
	// Method to add the Listeners
	private void listenerAdder()
	{
		// Left JButton
		leftButton.addActionListener(new ActionListener()
		{ public void actionPerformed(ActionEvent e) 
		{ mapScreen(); } } );
		// Middle JButton
		middleButton.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				goalScreen(); 
			} 
		});
		// Right JButton
		rightButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			} 
		});
	}
	
	// Methods to implement the Model behaviors :
	private void mapUpdater(String[] spots)
	{
		if (spots == null) { return; }
		JButton tempButton;
		int i = 0;
		while (i < spots.length)
		{
			tempButton = (JButton) Views.getInstance().getScene(-1).getParts()
					.get(Views.getInstance().getScene(-1).getSceneName() + spots[i]);
			if (!tempButton.isEnabled())
			{
				tempButton.setEnabled(true);
				tempButton.setVisible(true);
				if (spots[i].equals("Loc:1"))
				{
					tempButton.addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent e)
						{ screenChanger(Models.getInstance().getWindowModel().move("FirstScene")); }
					});
				}
				if (spots[i].equals("Loc:2"))
				{
					tempButton.addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent e)
						{ screenChanger(Models.getInstance().getWindowModel().move("SecondScene")); }
					});
				}
				if (spots[i].equals("Loc:3"))
				{
					tempButton.addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent e)
						{ screenChanger(Models.getInstance().getWindowModel().move("ThirdScene")); }
					});
				}
				if (spots[i].equals("Loc:4"))
				{
					tempButton.addActionListener(new ActionListener() 
					{
						public void actionPerformed(ActionEvent e)
						{ screenChanger(Models.getInstance().getWindowModel().move("FourthScene")); }
					});
				}
				
			}
			i = i + 1;
		}
	}
	
	// Method to control the Map screen printing
	private void mapScreen()
	{
		mapUpdater(Models.getInstance().getWindowModel().mapUpdater());
		if (Models.getInstance().getWindowModel().mapUpdater() == null) 
		{ return; }
		screenChanger(Models.getInstance().getWindowModel().mapPrinter());
	}
	public void forceMap()
	{
		mapUpdater(Models.getInstance().getWindowModel().mapUpdater());
		screenChanger(Models.getInstance().getWindowModel().mapForcedPrinter());
	}
	
	private void goalsUpdater(String[] goals)
	{
		//if (goals == null) { return; }
		
		for (JTextArea ta : 
			(JTextArea[]) Views.getInstance().getScene(-10).getParts().get(
					Views.getInstance().getScene(-10).getSceneName() + "TAs"))
		{
			if (!ta.isEnabled() && Arrays.asList(goals).contains(ta.getText()))
			{
				ta.setVisible(true);
				ta.setEnabled(true);
			}
			else if (ta.isEnabled() && !Arrays.asList(goals).contains(ta.getText()))
			{ ta.setForeground(new Color(85, 40, 85, 45)); }
		}
		/*
		JTextArea textA = (JTextArea) Views.getInstance().getScene(-10).getParts().get(
				Views.getInstance().getScene(-10).getSceneName() +  "TA");
		textA.setText(goalsText);
		*/
	}
	
	// Method to control the Goal screen printing
	private void goalScreen()
	{
		goalsUpdater(Models.getInstance().getWindowModel().goalsUpdater());
		if (exitableMap == false)
		{ return; }
		//screenChanger("Goals");
		screenChanger(Models.getInstance().getWindowModel().goalPrinter());
	}
	// Method to control the Scene JPanel scene change
	public void screenChanger(String sceneName)
	{
		if (sceneName.equals("Map"))
		{ displayedMap = true; }
		else
		{ displayedMap = false; }
		if (sceneName.equals("Goals"))
		{ displayedGoals = true; }
		else
		{ displayedGoals = false; }
		sceneLayout.show(scenePanel, sceneName);
		//currentScene = (Scene) uiParts.get(sceneName);
	}
	
}
