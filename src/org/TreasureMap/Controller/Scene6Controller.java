package org.TreasureMap.Controller;

// Package imports
import org.TreasureMap.Model.Models;
import org.TreasureMap.Model.Scene1Model;
// Imports Swing
import javax.swing.JButton;

public class Scene6Controller extends SceneController {

	//private Scene1Model model;
	private JButton button;
	//private BiModel leader;
	
	Scene6Controller()
	{
		// SceneController constructor
		super();
		// Initialize the scene index and name
		sceneIdx = 6;
		sceneNameRetriever(sceneIdx);
		// Initialize the UI components and listeners
		button = buttonAssigner();
		//model = (Scene1Model) Models.getInstance().getSceneModel(sceneIdx);
	}

	// // // // // METHODS
	// Parent method overrides
	@Override
	protected void validityCheck()
	{

		
	}
	
}
