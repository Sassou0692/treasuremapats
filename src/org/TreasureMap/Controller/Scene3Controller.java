package org.TreasureMap.Controller;

// Package imports
import org.TreasureMap.Model.Models;
import org.TreasureMap.Model.Scene1Model;
// Imports Swing
import javax.swing.JButton;

public class Scene3Controller extends SceneController {

	//private Scene3Model model;
	private JButton button;
	//private BiModel leader;
	
	Scene3Controller()
	{
		// SceneController constructor
		super();
		// Initialize the scene index and name
		sceneIdx = 3;
		sceneNameRetriever(sceneIdx);
		// Initialize the UI components and listeners
		button = buttonAssigner();
		//model = (Scene3Model) Models.getInstance().getSceneModel(sceneIdx);
	}

	// // // // // METHODS
	// Parent method overrides
	@Override
	protected void validityCheck()
	{
				Models.getInstance().getWindowModel().progression(
						Models.getInstance().getSceneModel(sceneIdx).itemChecker());
		
			button.setVisible(false);
			button.setEnabled(false);
			button.removeAll();
			Controllers.getInstance().getWindowController().forceMap();
	}
	
}
