package org.TreasureMap.Controller;

import org.TreasureMap.View.Scene;
import org.TreasureMap.View.Views;
import org.TreasureMap.Model.Models;
import org.TreasureMap.Model.TiBiModel;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.util.HashMap;

public abstract class SceneController {
	
	//protected WindowController windowController;
	//protected HashMap<String, Object> parts;
	protected int sceneIdx;
	protected String sceneName;
	protected String textAKey;
	protected String dialogsKey;
	protected String inputFKey;
	protected String buttonKey;
	
	SceneController(/*WindowController windCont, Scene scn*/)
	{
		//windowController = windCont;
		//parts = scn.getParts();
		//sceneName = (String) parts.get("SceneName");
		
		textAKey = "TA";
		dialogsKey = "Dialogs";
		inputFKey = "IF";
		buttonKey = "Validate";
	}
	
	protected void sceneNameRetriever(int idx)
	{ sceneName = Views.getInstance().getScene(idx).getSceneName(); }
	
	protected JTextField inputFAssigner()
	{
		JTextField inputF = (JTextField) Views.getInstance().getScene(sceneIdx).getParts().get(sceneName + inputFKey);
		inputFListening(inputF);
		return inputF;
	}
	
	protected JButton buttonAssigner()
	{
		JButton button = (JButton) Views.getInstance().getScene(sceneIdx).getParts().get(sceneName + buttonKey);
		buttonListening(button);
		return button;
	}
	
	protected String inputFRetriever(JTextField inputF)
	{ return inputF.getText(); }
	
	protected void rewriteTextA(String text)
	{ }
	
	private void cleanInputF(JTextField inputF)
	{ inputF.setText(""); }
	
	protected void reassignInputF(JTextField inputF)
	{ }
	
	// Methods to Override in child classes :
	// Method of String processing to override in child classes
	protected void strValueCheck(String value)
	{ }
	// Method of progression processing to override in child classes
	protected void validityCheck()
	{ }
	
	protected void textAListening(JTextArea textA)
	{
		textA.addMouseListener(new MouseListener() 
				{
			public void mouseEntered(MouseEvent e) { }
			public void mouseExited(MouseEvent e) { }
			public void mousePressed(MouseEvent e) { }
			public void mouseReleased(MouseEvent e) { }
			public void mouseClicked(MouseEvent e) { }
				});
	}
	
	protected void inputFListening(JTextField inputF)
	{
		inputF.addKeyListener(new KeyListener()
				{
			public void keyPressed(KeyEvent e) { }
			public void keyReleased(KeyEvent e) { }
			public void keyTyped(KeyEvent e) 
			{ strValueCheck(inputFRetriever(inputF)); }
				});
		inputF.addMouseListener(new MouseListener() 
				{
			public void mouseEntered(MouseEvent e) { }
			public void mouseExited(MouseEvent e)
			{ reassignInputF(inputF); }
			public void mousePressed(MouseEvent e) { }
			public void mouseReleased(MouseEvent e) { }
			public void mouseClicked(MouseEvent e) 
			{ cleanInputF(inputF); }
				});
	}
	
	protected void buttonListening(JButton button)
	{
		button.addActionListener(new ActionListener()
				{
			public void actionPerformed(ActionEvent e) 
			{ validityCheck(); }
				});
	}
	
}

/*
class TextInButtonIn extends SceneController {
	
	private JTextField inputF;
	private JButton button;
	private TiBiModel leader;
	
	TextInButtonIn(WindowController windCont, Scene scn)
	{
		super(windCont, scn);
		
		leader = Models.getInstance().getTiBiModel();
		
		inputF = (JTextField) parts.get(sceneName + inputFKey);
		inputFListening(inputF);
		button = (JButton) parts.get(sceneName + buttonKey);
		buttonListening(button);
	}
	 @Override
	protected void reassignInputF(JTextField inputF)
	{
		 if (inputFRetriever(inputF).equals(""))
		 { inputF.setText("Name ?"); }
	}
	
	@Override
	protected void validityCheck()
	{
		windowController.screenChanger(
				leader.progression(
						leader.textChecker(
								inputFRetriever(inputF), true)));
	}

}

class ButtonIn extends SceneController 
{
	private JButton button;
	//private BiModel leader;
	
	ButtonIn(WindowController windCont, Scene scn)
	{
		super(windCont, scn);
		// leader = Models.getInstance().getBiModel();
	
		button = (JButton) parts.get(sceneName + buttonKey);
		buttonListening(button);
	}
	@Override
	protected void validityCheck()
	{
		windowController.screenChanger("SecondScene");
	}

}

class SecondSceneController extends SceneController  {
	
	private JTextArea textA;
	private String[] texts;
	private JTextField inputF;
	private JButton button;
	private int dialogIdx;
	//private BiModel leader;
	
	SecondSceneController(WindowController windCont, Scene scn)
	{
		super(windCont, scn);
		// leader = Models.getInstance().getBiModel();
		
		textA = (JTextArea) parts.get(sceneName + textAKey);
		texts = (String[]) parts.get(sceneName + dialogsKey);
		//textAListening(textA);
		inputF = (JTextField) parts.get(sceneName + inputFKey);
		inputFListening(inputF);
		button = (JButton) parts.get(sceneName + buttonKey);
		buttonListening(button);
		dialogIdx = 0;
	}
	
	@Override
	protected void rewriteTextA(String text)
	{ textA.setText(text); }
	
	@Override
	protected void validityCheck()
	{
		dialogIdx = dialogIdx + 1;
		if (dialogIdx < texts.length)
		{ rewriteTextA(texts[dialogIdx]); }
		else
		{
			dialogIdx = dialogIdx - 1;
			Controllers.getInstance().getWindowController().screenChanger(
					Models.getInstance().getSceneModel(2).progression(
							Models.getInstance().getSceneModel(2).textChecker(
									inputFRetriever(inputF))));
		}
	}

}
*/
